MODULE Berardo
    
    PERS tasks arms_sync{2} := [ ["T_ROB_L"], ["T_ROB_R"] ];
    VAR syncident task3_sync;
    VAR syncident task4_sync1;
    VAR syncident task4_sync2;
    VAR syncident task4_sync3;
    
    ! For all tasks
    CONST robtarget OverPlaceIndexR:=[[396.87,-14.86,155.00],[0.0508324,0.213518,-0.975537,0.0123798],[0,-1,1,4],[-114.162,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget PlaceIndexLegoR:=[[396.81,-14.81,-12.34],[0.0509518,0.213644,-0.975504,0.0123205],[0,-1,1,4],[-108.049,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    !For task1 and task2
    CONST robtarget OverPalletR:=[[61.63,119.87,192.74],[0.0508931,0.213538,-0.97553,0.0123714],[1,-1,1,4],[-116.404,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget PalletPosR:=[[61.62,119.86,7.80],[0.0508915,0.213533,-0.975531,0.0123869],[1,-2,1,4],[-116.403,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    ! For task3 and task4
    CONST robtarget OverCenterPalletR:=[[2.66,34.88,166.87],[0.042529,0.178934,-0.982909,0.00797594],[1,-2,2,4],[-122.772,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget PalletCenterPosR :=[[2.64,34.89,44.44],[0.0425562,0.178921,-0.98291,0.00798295],[1,-2,1,4],[-120.6,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    !For task4
    CONST robtarget PalletHigh:=[[12.58,25.71,197.75],[0.139949,0.774383,0.104001,-0.608218],[1,0,2,4],[166.657,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget PalletHighRight :=[[232.84,25.70,197.75],[0.139955,0.774383,0.104001,-0.608217],[0,0,2,4],[166.656,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget PalletPosT4 :=[[0.38,26.77,22.12],[0.048252,0.170182,-0.984133,0.0138676],[1,-2,1,4],[-122.908,9E+09,9E+09,9E+09,9E+09,9E+09]];

    
PROC task1()
    Hand_GripInward;
    MoveAbsJ jpHomeR\NoEOffs,v200,z50,tool0\WObj:=wobj0;
    MoveJ OverPlaceIndexR,v200,z50,tGripper\WObj:=wRightBuffer;
    Hand_GripOutward;
    MoveJ PlaceIndexLegoR,v200,fine,tGripper\WObj:=wRightBuffer;
    Hand_GripInward;
    MoveJ OverPlaceIndexR,v200,z50,tGripper\WObj:=wRightBuffer;
    MoveJ OverPalletR,v200,z50,tGripper\WObj:=wPalletIndex;
    MoveJ PalletPosR,v200,fine,tGripper\WObj:=wPalletIndex;
    Hand_GripOutward;
    MoveJ OverPalletR,v200,z50,tGripper\WObj:=wPalletIndex;
    MoveAbsJ jpHomeR\NoEOffs,v200,z50,tool0\WObj:=wobj0;    
    Hand_GripInward;
ENDPROC

PROC task2()
    task1;
ENDPROC

PROC task3()
    WaitSyncTask task3_sync, task_list;
    Hand_GripInward;
    MoveAbsJ jpHomeR\NoEOffs,v200,z50,tool0\WObj:=wobj0;
    MoveJ OverPlaceIndexR,v200,z50,tGripper\WObj:=wRightBuffer;
    Hand_GripOutward;
    MoveJ PlaceIndexLegoR,v200,fine,tGripper\WObj:=wRightBuffer;
    Hand_GripInward;
    MoveJ OverPlaceIndexR,v200,z50,tGripper\WObj:=wRightBuffer;
    MoveJ OverCenterPalletR,v200,z50,tGripper\WObj:=wPalletIndex;
    MoveJ PalletCenterPosR,v200,fine,tGripper\WObj:=wPalletIndex;
    Hand_GripOutward;
    MoveJ OverCenterPalletR,v200,z50,tGripper\WObj:=wPalletIndex;
    MoveAbsJ jpHomeR\NoEOffs,v200,z50,tool0\WObj:=wobj0;    
    Hand_GripInward;
ENDPROC

PROC task4()
    Hand_GripInward;
    MoveAbsJ jpHomeR\NoEOffs,v200,z50,tool0\WObj:=wobj0;
    WaitSyncTask task4_sync1, arms_sync;
    MoveJ PalletHighRight,v200,z50,tGripper\WObj:=wPalletIndex;
    Hand_GripOutward;
    MoveJ PalletHigh,v200,fine,tGripper\WObj:=wPalletIndex;
    Hand_GripInward;
    WaitSyncTask task4_sync2, task_list;
    WaitSyncTask task4_sync3, task_list;
    MoveJ OverCenterPalletR,v200,z50,tGripper\WObj:=wPalletIndex;
    MoveJ PalletPosT4,v200,fine,tGripper\WObj:=wPalletIndex;
    Hand_GripOutward;
    MoveJ OverCenterPalletR,v200,z50,tGripper\WObj:=wPalletIndex;
    MoveAbsJ jpHomeR\NoEOffs,v200,z50,tool0\WObj:=wobj0;
    Hand_GripInward;
ENDPROC

PROC main_berardo()
    Hand_init;
    !task2;
    !task3;
    task4;
ENDPROC

ENDMODULE