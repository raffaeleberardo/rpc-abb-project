MODULE Module1
    PERS tasks task_list{2} := [ ["T_ROB_L"], ["T_ROB_R"] ];
    VAR syncident task3_sync;
    VAR syncident task4_sync1;
    VAR syncident task4_sync2;
    VAR syncident task4_sync3;
    CONST robtarget HomePositionR:=[[89.388078679,-156.622316462,147.913495309],[0.066010716,-0.842420933,-0.11121496,-0.523068629],[0,-2,2,5],[-110.769230769,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget OverBoxR:=[[8,-16,200],[0,1,0,0],[0,2,-2,4],[-118.321678322,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget BoxPositionR:=[[8,-16,6.5],[0,1,0,0],[0,2,-2,4],[-118.321678322,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget OverTable:=[[98.919,-156.087,200],[0,0,1,0],[1,2,0,4],[-110.769230769,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget TablePosition:=[[98.919,-156.087,46.5],[0,0,1,0],[1,2,0,4],[-110.769230769,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget OverCenterTable:=[[98.919,-101.087,200],[0,1,0,0],[1,2,-2,4],[-118.321678322,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget CenterTable:=[[98.919,-101.087,53],[0,1,0,0],[1,2,-2,4],[-110.769230769,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget CenterTableHigh:=[[98.919,-151.087,253],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-110.769230769,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget CenterTableHigh_2:=[[98.919,-101.087,253],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-110.769230769,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget CenterTable_3:=[[98.919,-101.087,46.5],[0,0.707106781,0.707106781,0],[1,-2,1,4],[-110.769230769,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget OverCenterTable_2:=[[98.919,-101.087,200],[0,0.707106781,0.707106781,0],[1,-2,1,4],[-110.769230769,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PROC PickAndPlaceToPlatform()
        SetDO OpenGripR,0;
        SetDO CloseGripR,0;
        WaitDI StartSim,1;
        MoveJ HomePositionR,v200,fine,Servo\WObj:=ArmR;
        MoveJ OverBoxR,v200,fine,Servo\WObj:=LegoBrickR;
        SetDO OpenGripR,1;
        WaitDI GripOpenedR,1;
        SetDO OpenGripR,0;
        MoveJ BoxPositionR,v200,fine,Servo\WObj:=LegoBrickR;
        SetDO CloseGripR,1;
        WaitDI GripClosedR,1;
        SetDO CloseGripR,0;
        MoveJ OverBoxR,v200,fine,Servo\WObj:=LegoBrickR;
        MoveJ OverTable,v200,fine,Servo\WObj:=Table;
        MoveJ TablePosition,v200,fine,Servo\WObj:=Table;
        SetDO OpenGripR,1;
        WaitDI GripOpenedR,1;
        SetDO OpenGripR,0;
        MoveJ OverTable,v200,fine,Servo\WObj:=Table;
        MoveJ HomePositionR,v200,fine,Servo\WObj:=ArmR;
        SetDO CloseGripR,1;
    ENDPROC
    PROC Task2()
        PickAndPlaceToPlatform;
    ENDPROC
    PROC EmptyPath()
    ENDPROC
    PROC Task3()
        WaitSyncTask task3_sync, task_list;
        SetDO OpenGripR,0;
        SetDO CloseGripR,0;
        MoveJ HomePositionR,v100,fine,Servo\WObj:=ArmR;
        MoveJ OverBoxR,v100,fine,Servo\WObj:=LegoBrickR;
        SetDO OpenGripR,1;
        WaitDI GripOpenedR,1;
        SetDO OpenGripR,0;
        MoveJ BoxPositionR,v100,fine,Servo\WObj:=LegoBrickR;
        SetDO CloseGripR,1;
        WaitDI GripClosedR,1;
        SetDO CloseGripR,0;
        MoveJ OverBoxR,v100,fine,Servo\WObj:=LegoBrickR;
        MoveJ CenterTableHigh_2,v100,fine,Servo\WObj:=Table;
        MoveJ OverCenterTable,v100,fine,Servo\WObj:=Table;
        MoveJ CenterTable,v100,fine,Servo\WObj:=Table;
        SetDO OpenGripR,1;
        WaitDI GripOpenedR,1;
        SetDO OpenGripR,0;
        MoveJ OverCenterTable,v100,fine,Servo\WObj:=Table;
        MoveJ HomePositionR,v100,fine,Servo\WObj:=ArmR;
        SetDO CloseGripR,1;
    ENDPROC
    PROC Task4()
        SetDO OpenGripR,0;
        SetDO CloseGripR,0;
        WaitSyncTask task4_sync1, task_list;
        MoveJ HomePositionR,v100,fine,Servo\WObj:=ArmR;
        MoveJ CenterTableHigh,v100,fine,Servo\WObj:=Table;
        SetDO OpenGripR,1;
        WaitDI GripOpenedR,1;
        SetDO OpenGripR,0;
        MoveJ CenterTableHigh_2,v100,fine,Servo\WObj:=Table;
        SetDO CloseGripR,1;
        WaitDI GripClosedR,1;
        SetDO CloseGripR,0;
        WaitSyncTask task4_sync2, task_list;
        WaitSyncTask task4_sync3, task_list;
        MoveJ CenterTableHigh,v100,fine,Servo\WObj:=Table;
        MoveJ OverCenterTable_2,v100,fine,Servo\WObj:=Table;
        MoveJ CenterTable_3,v100,fine,Servo\WObj:=Table;
        SetDO OpenGripR,1;
        WaitDI GripOpenedR,1;
        SetDO OpenGripR,0;
        MoveJ OverCenterTable_2,v100,fine,Servo\WObj:=Table;
        MoveJ CenterTableHigh_2,v100,fine,Servo\WObj:=Table;
        MoveJ HomePositionR,v100,fine,Servo\WObj:=ArmR;
        SetDO CloseGripR,1;
    ENDPROC
ENDMODULE