MODULE CalibData
    PERS tooldata Servo:=[TRUE,[[0,0,134.2],[1,0,0,0]],[0.24,[8.2,12.5,48.1],[1,0,0,0],0.00022,0.00024,0.00009]];
    PERS tooldata VaccumOne:=[TRUE,[[63.5,18.5,37.5],[0.707106781,0,0.707106781,0]],[0.24,[8.2,12.5,48.1],[1,0,0,0],0.00022,0.00024,0.00009]];
    PERS tooldata Camera:=[TRUE,[[-7.3,28.3,35.1],[0.5,-0.5,0.5,0.5]],[0.24,[8.2,12.5,48.1],[1,0,0,0],0.00022,0.00024,0.00009]];
    TASK PERS wobjdata LegoBrickR:=[FALSE,TRUE,"",[[0,-363,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata Table:=[FALSE,TRUE,"",[[301.081,101.087,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    TASK PERS wobjdata ArmR:=[FALSE,TRUE,"",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
ENDMODULE