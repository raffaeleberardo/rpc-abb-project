MODULE Module1
    PERS tasks task_list{2} := [ ["T_ROB_L"], ["T_ROB_R"] ];
    VAR syncident task3_sync;
    VAR syncident task4_sync1;
    VAR syncident task4_sync2;
    VAR syncident task4_sync3;
    CONST robtarget HomePositionL:=[[89.388099795,156.622559942,147.913520613],[0.066010673,0.8424211,-0.111215152,0.523068324],[0,0,0,4],[101.964435023,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget OverBoxL:=[[8,-16,200],[0,1,0,0],[0,1,-2,4],[101.964435023,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget BoxLPosition:=[[8,-16,6.5],[0,1,0,0],[0,1,-2,4],[101.964435023,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget OverTable:=[[98.919,-26.087,200],[0,0,1,0],[-1,2,0,4],[101.964430474,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget TablePosition:=[[98.919,-26.087,46.5],[0,0,1,0],[-1,2,0,4],[101.964430474,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget OverCenterTable:=[[98.919,-101.087,180],[0,1,0,0],[-1,2,-2,4],[101.964430992,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget CenterTable:=[[98.919,-101.087,46.5],[0,1,0,0],[-1,2,-2,4],[101.964430992,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget CenterTableHigh:=[[98.919,-51.087,253],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964435023,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget CenterTableHigh_2:=[[98.919,-101.087,253],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964435023,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PROC PickAndPlaceToPlatform1()
        SetDO OpenGripL,0;
        SetDO CloseGripL,0;
        WaitDI StartSim,1;
        MoveJ HomePositionL,v200,fine,Servo\WObj:=ArmL;
        MoveJ OverBoxL,v200,fine,Servo\WObj:=LegoBrickL;
        SetDO OpenGripL,1;
        WaitDI GripOpenedL,1;
        SetDO OpenGripL,0;
        MoveJ BoxLPosition,v200,fine,Servo\WObj:=LegoBrickL;
        SetDO CloseGripL,1;
        WaitDI GripClosedL,1;
        SetDO CloseGripL,0;
        MoveJ OverBoxL,v200,fine,Servo\WObj:=LegoBrickL;
        MoveJ OverTable,v200,fine,Servo\WObj:=Table;
        MoveJ TablePosition,v200,fine,Servo\WObj:=Table;
        SetDO OpenGripL,1;
        WaitDI GripOpenedL,1;
        SetDO OpenGripL,0;
        MoveJ OverTable,v200,fine,Servo\WObj:=Table;
        MoveJ HomePositionL,v200,fine,Servo\WObj:=ArmL;
        SetDO CloseGripL,1;
        WaitDI GripClosedL,1;
        SetDO CloseGripL,0;
    ENDPROC
    PROC PickAndPlaceToPlatform()
        SetDO OpenGripL,0;
        SetDO CloseGripL,0;
        WaitDI StartSim,1;
        MoveJ HomePositionL,v200,fine,Servo\WObj:=ArmL;
        MoveJ OverBoxL,v200,fine,Servo\WObj:=LegoBrickL;
        SetDO OpenGripL,1;
        WaitDI GripOpenedL,1;
        SetDO OpenGripL,0;
        MoveJ BoxLPosition,v200,fine,Servo\WObj:=LegoBrickL;
        SetDO CloseGripL,1;
        WaitDI GripClosedL,1;
        SetDO CloseGripL,0;
        MoveJ OverBoxL,v200,fine,Servo\WObj:=LegoBrickL;
        MoveJ OverTable,v200,fine,Servo\WObj:=Table;
        MoveJ TablePosition,v200,fine,Servo\WObj:=Table;
        SetDO OpenGripL,1;
        WaitDI GripOpenedL,1;
        SetDO OpenGripL,0;
        MoveJ OverTable,v200,fine,Servo\WObj:=Table;
        MoveJ HomePositionL,v200,fine,Servo\WObj:=ArmL;
        SetDO CloseGripL,1;
    ENDPROC
    PROC Task1()
        PickAndPlaceToPlatform;
    ENDPROC
    PROC Task2()
        PickAndPlaceToPlatform;
    ENDPROC
    PROC Task3()
        SetDO OpenGripL,0;
        SetDO CloseGripL,0;
        WaitDI StartSim,1;
        MoveJ HomePositionL,v100,fine,Servo\WObj:=ArmL;
        MoveJ OverBoxL,v100,fine,Servo\WObj:=LegoBrickL;
        SetDO OpenGripL,1;
        WaitDI GripOpenedL,1;
        SetDO OpenGripL,0;
        MoveJ BoxLPosition,v100,fine,Servo\WObj:=LegoBrickL;
        SetDO CloseGripL,1;
        WaitDI GripClosedL,1;
        SetDO CloseGripL,0;
        MoveJ OverBoxL,v100,fine,Servo\WObj:=LegoBrickL;
        MoveJ CenterTableHigh_2,v100,fine,Servo\WObj:=Table;
        MoveJ CenterTable,v100,fine,Servo\WObj:=Table;
        SetDO OpenGripL,1;
        WaitDI GripOpenedL,1;
        SetDO OpenGripL,0;
        MoveJ CenterTableHigh_2,v100,fine,Servo\WObj:=Table;
        MoveJ HomePositionL,v100,fine,Servo\WObj:=ArmL;
        SetDO CloseGripL,1;
        WaitSyncTask task3_sync, task_list;
    ENDPROC
    PROC Task4()
        SetDO OpenGripL,0;
        SetDO CloseGripL,0;
        WaitDI StartSim,1;
        MoveJ HomePositionL,v100,fine,Servo\WObj:=ArmL;
        MoveJ OverBoxL,v100,fine,Servo\WObj:=LegoBrickL;
        SetDO OpenGripL,1;
        WaitDI GripOpenedL,1;
        SetDO OpenGripL,0;
        MoveJ BoxLPosition,v100,fine,Servo\WObj:=LegoBrickL;
        SetDO CloseGripL,1;
        WaitDI GripClosedL,1;
        SetDO CloseGripL,0;
        MoveJ OverBoxL,v100,fine,Servo\WObj:=LegoBrickL;
        MoveJ CenterTableHigh_2,v100,fine,Servo\WObj:=Table;
        WaitSyncTask task4_sync1, task_list;
        WaitSyncTask task4_sync2, task_list;
        SetDO OpenGripL,1;
        WaitDI GripOpenedL,1;
        SetDO OpenGripL,0;
        MoveJ CenterTableHigh,v100,fine,Servo\WObj:=Table;
        WaitSyncTask task4_sync3, task_list;
        MoveJ HomePositionL,v100,fine,Servo\WObj:=ArmL;
        SetDO CloseGripL,1;
    ENDPROC
ENDMODULE