# Robotics Programming And Control - Project Course
## Introduction - ABB RobotStudio
RobotStudio is a PC application made to create, program and simulate robot cells and stations.
The following tasks are programmed on a Yumi IRB 14000 both in the simulated and on the real environment:

1. Pick of a cube and put it in front of the robot
2. Pick of two cubes, one for each arm, and put them in front of the robot simultaneously
3. Pick of a cube and put it in front of the robot, than with the other arm pick another cube and put it on top of the other
4. Pick of a cube with the left arm, put it at mid air, grab it with the other arm and put it in front of the robot

# Simulation

## Importing Yumi Robot

To import the Yumi Robot we have to go in *Home - ABB Library - Collaborative Robots* and click on the Yumi Robot which is present in this category.
We will have the robot without the grippers. 

To mount the 2 grippers we have to go in *Home - Import Library - Equipment - Tools* and click on the ABB Smart Gripper.
 Then we have to select the type of grippers where we will have a gripper with the features *Servo, Camera, Vaccum* for the right arm and a gripper with features *Servo, Vacuum* for the left one.
 Now we have the robot completely mounted.

 To control the Yumi robot we also have to import its controller which we can import via *Home - Virtual Controller - New Controller* and select the correct robot (*IRB 14000 05kg 0.5m*) and the controller *IRC5 v6.14.00.01*.

## Environment

To simulate the above tasks a simple environment is created in the ABB RobotStudio.
A small cube which simulates a simple **lego brick** and a larger paralleleiped which simulates the real **ICE Laboratory pallet** are made by using the *Solid* option in the *Modeling* tab which we find on the top of the software. Here we specify some measures which are coherent with the real environment.

In the end we define 2 small bricks and a larger table to have the simulated environment.

<center>
<img src="img/simulation_start.png" width = 80%>
</center>

## Workobjects
A workobject is a coordinate system used to describe the position of a work piece. The workobject consists of two frames: a user frame and an object frame. All programed positions will be related to the object frame, which is related to the user frame, which is related to the world coordinate system.
To define them we have to go to *Home - Other (in the Path Programming tab) - Create workobject*. Here we can define the workobject name and then its position (use the *Snap Object* option in the **top view bar** to select a point precisely).

For this project there are 3 defined workobjects for the right arm and other 3 workobjects for the left arm.
The workobjects for the right arm are:
- ArmR : define the right arm coordinate system
- LegoBrickR : define the right orange cube coordinate system
- Table : define the table coordinate system

The workobjects for the left arm are mainly the same but for the left arm, so they are:

- ArmL : define the left arm coordinate system
- LegoBrickL : define the left red cube coordinate system
- Table : define the table coordinate system

The table and cubes coordinate systems are put in their bottom left corner.
To let the workobject move with the object we are referring to right click on the interested workobject and select *Attach to - model to attach the workobject to*.

<center>
<img src="img/workobjects.png">
</center>

## Targets
A Target is compose by coordinates that the robot shall reach. It contains information about its position with respect to a workobject coordinate system, the orientation relative to the workobject orientation and the configuration which specify how the robot shall reach the target.

For this project we defined multiple targets both for the left and right arm.
To define them right click on the workobject to refer to and then *Create Target*.
The targets for the arms are just their **Home pose**, for the cubes the target are a **pose over the cube and the initial cube pose**.

Here are represented **all the targets** for right and left arm.

<center>
<img src="img/right_targets.png" height = 250px>
<img src="img/left_targets.png" height = 250px>
</center>

## Paths
To define a path we can right click on the folder *Paths & Procedure* for the arm we are interested in and then select *Create path*.
After giving a name to the path we can drag the targets we want to reach. 
The important thing is to put the targets in the order you want to reach them.
To be sure to reach the targets with a correct confguration we used the *Auto Configuration - Linear/Circular move instructions* and selected a correct configuration to allow the robot to perform a smooth movement along the path.
We can see if the trajectory is correct by right click on the defined path and selecting *Move along Path*. The gripper will not move because to do this we have to define some signals and behaviours to command the gripper.

## Signals

To define a signal we have to go in the *Controller - Configuration - I/O System - Signal* and right click on the table and click on the *New Signal* option. Here we can assign all the features we want for our signals.

We defined some **Digital output** and **Digital input** signals which allow the Yumi to know when to open and close the right or left gripper.
The signals for the left gripper finish with an L at the end of their nomenclature while with an R for the right gripper nomenclatures.

The defined signals for the grippers are the following one:
1. OpenGripL/R - Digital Output signal which command the grip opening
2. CloseGripL/R - Digital Output signal which command the grip closure
3. GripOpenedL/R - Digital Input which advertises the grip finish to open
4. GripClosedL/R - Digital Input which advertises the grip finish to close

A digital input signal is defined to start the simulation by one click. It is the *StartSim* signal.

To add a signal with its value in a path we can right click on the path we are interested in and the select *Insert Action Instruction* where we can select the signal with a corresponding value.

Here we can see an example of the path executed for the **first pick and place** for the left arm.

<center>
<img src="img/task1.png" height = 400px>
</center>

To see if the behaviour of the path with the setted signal is correct we can select *Simulation - I/O Simulator* and then select the right controller in the *Select Controller* tab (in our case IRB14000) and then set the signals to 0 or 1 by selecting them from the *Filter* tab. 
To see if the behaviour is correct follow the order of the defined signals in the path, setting the signals to the defined value will move the robot.
Here we can see if the signals are correct but the gripper will not move yet.

## Gripper Movement and Grabbing Simulation
To set an opening position for the grippers we can go to *Layout*, select the gripper we are interested in, right click and select *Mechanism Jog*. Here we can set how much we want to open the gripper. For the project we want a full opened grip.

Then we can add these poses for the grippers as open by going to the *Mechanism Tools - Modify - Move To Pose - Edit Poses* and add the current pose of the gripper, giving it a name.

We can simulate the real behaviour of the Yumi Robot using setting up the *Station Logic* window. To open the station logic window we can select *Simulation - Station Logic*.
Here we simulated the behaviour for opening/closing the gripper and the grabbing behaviour between the gripper and the cube, to avoid the gripper to pass through a cube (in real life this is not needed).

Here we can see all the components which simulates a real life behaviour of our tasks.
<center>
<img src="img/station_logic.png">
</center>

The main components are:
1. **PoseMover**: when the execute input signal is set the mechanism joint values are moved to the given pose. Here we can specify the mechanism we want to move and the pose to move to with also a defined duration of the movement. This is mainly use to open and close the gripper when receiving the corresponding signal defined above.
2. **Attacher**: will attach the child to parent when the execute signal is set. Simulates a cube attached to the gripper, so the grapping behaviour. The output executed will be set when finished. When we close the gripper, we want to attach the cube to it when a collision is detected.
3. **Detacher**: will detach the child from the object it is attach to when the execute signal is set. When finished the executed signal is set. When we open the gripper we want to detach the cube.
4. **CollisionSensor**: detects collisions and near miss events between the first object and the second object. When the active signal is high and a collision event occurs and the component is active, the SensorOut signal is set and the parts that participate in the collision event are reported in the first colliding part and second colliding part of the property editor. This component allow us to detect a collision between the gripper and the cube and then stop the closing movement of the gripper, allowing the cube grasping.

Once we set up the station logic we are ready to perform the defined trajectory for all the tasks.

## RAPID Code and Arms Synchronization
Once completed the above steps we can generate the RAPID Code to command the robot by right clicking on one path and selecting *Synchronize to RAPID* where we can convert all the defined tasks to RAPID code.
Here a picture of how a RAPID code for the first pick and place looks like.

<center>
<img src="img/RAPID_simulation.png">
</center>

In the RAPID code we can set to perform a linear movement or a joint movement by using respectively the **MoveL and MoveJ** instructions. Then we can set the target to move with a given velocity defined by **v<velocity_value>** and a zone which is a range in which we can move if we cannot reach the point exactly in the setted pose. In the simulation the *fine* zone value define that we want to reach exactly the defined point with no errors.

To synchronize the arms we can use the instructions **WaitSyncTask <task_name>, <task_list>** which is used  to synchronize several program tasks at a special point in each
program. Each program task waits until all program tasks have reach the named
synchronization point.
To use **WaitSyncTask** we also have to define a task list which contains the tasks we have to synchronize, in our case the right and left arm (T_ROB_R and T_ROB_L).
In the end we define some syncident (synchronization identity) which are used to specify the name of a synchronization point.
The name of the synchronization point will be the name (identity) of the declared data of type
syncident.
Here an example of how to define a task list with its syncident.

<center>
<img src="img/syncident.png">
</center>

It is important to know that we have to define the task list and the syncident in the **global** scope.
Here a task with the WaitTask instruction.

<center>
<img src="img/RAPID_Wait.png">
</center>

# Deployment on the Real Robot
Once we understood how to write a RAPID program we can bring the logic to the real environment where we have mainly to change the instructions for opening the gripper and the targets we want to bring the robot to.

Once we correct the parts of program we are interested in, we can use the *ABB FlexPendant* to set the values for the defined variables in the RAPID procedures which are my targets.
Here I modified also some velocities and some zones to have a smoother path without errors that may be caused when near to singularities.
It is important to initialize the grippers and to use the correct commands to open and close them. This to avoid to scalibrate the robot configuration.

To specify a target we used the *ABB FlexPendant* to bring the robot to a desired target in linear motion, reorienting the gripper and/or the arms as we want to, then we can store the pose on the desired variable which are brought from the PC application to the real controller and so they are visualizable on the FlexPendant.

We can find the RAPID code in the `Controller Data\UNIVR-ICE-ABB-YUMI\RAPID` path.

# Results
[Click here to see the video results](https://univr-my.sharepoint.com/:f:/g/personal/raffaele_berardo_studenti_univr_it/EkFH5SjPJDhAofcUSgWph4kB9cov6TpfCBrfg_5l02-b3A?e=c2ePzP)
