MODULE log_rob_l
    
    PERS num LOGGER_PERIOD:=0.01;
    
    PROC id_main()
    	VAR string txt;
        VAR string pos_str:="";
        VAR string pose_str:="";
        VAR string speed_str:="";
        VAR string torque_str:="";
        VAR string motor_torque_str:="";
    	VAR num position{7};
        VAR num speed{7};
    	VAR num torques{7};
        VAR num motor_torque{7};
        VAR num extTorques{7};
    	VAR string time;
        VAR iodev file1_l;
        VAR iodev file2_l;
        VAR iodev file3_l;
        VAR iodev file4_l;
        VAR iodev file_pose;
        VAR iodev file_pose_pallet;
        VAR iodev file_vac_base;
        VAR iodev file_vac_pallet;
        VAR bool exit_cycle := TRUE;
        VAR num counter:=0;
        VAR num type_control:=1;
        VAR robtarget target_pos;
    
        Open "HOME:" \File:= "/ROS_MODULES/Identification/pos_arm_arm_l.txt", file1_l \Write;  
    	Open "HOME:" \File:= "/ROS_MODULES/Identification/speed_arm_arm_l.txt", file2_l \Write; 
        Open "HOME:" \File:= "/ROS_MODULES/Identification/torque_arm_arm_l.txt", file3_l \Write; 
        Open "HOME:" \File:= "/ROS_MODULES/Identification/motor_torque_arm_arm_l.txt", file4_l \Write;
        Open "HOME:" \File:= "/ROS_MODULES/Identification/pose_l.txt", file_pose \Write;
        Open "HOME:" \File:= "/ROS_MODULES/Identification/pos_arm_pallet_l.txt", file_pose_pallet \Write;
        Open "HOME:" \File:= "/ROS_MODULES/Identification/pos_vac_pallet_l.txt", file_vac_pallet \Write;
        Open "HOME:" \File:= "/ROS_MODULES/Identification/pos_vac_base_l.txt", file_vac_base \Write;
    	time:= CTime();
        
        WaitTime 1;
    	WHILE TRUE DO

            ! position of tool0 wrt wobj0
            pose_str:="";
            target_pos := CRobT(\Tool:=tool0, \WObj:=wobj0);
            pose_str := pose_str + NumToStr(target_pos.trans.x ,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.trans.y,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.trans.z,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q1,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q2,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q3,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q4,4) + ";";
            pose_str := pose_str;
            Write file_pose, pose_str;
            
            ! position of tool0 wrt pallet
            pose_str:="";
            target_pos := CRobT(\Tool:=tool0, \WObj:=wPalletIndex);
            pose_str := pose_str + NumToStr(target_pos.trans.x ,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.trans.y,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.trans.z,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q1,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q2,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q3,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q4,4) + ";";
            pose_str := pose_str;
            Write file_pose_pallet, pose_str;
            
            ! position of suction wrt base
            pose_str:="";
            target_pos := CRobT(\Tool:=tVacuum1, \WObj:=wobj0);
            pose_str := pose_str + NumToStr(target_pos.trans.x ,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.trans.y,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.trans.z,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q1,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q2,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q3,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q4,4) + ";";
            pose_str := pose_str;
            Write file_vac_base, pose_str;
            
            ! position of suction wrt pallet
            pose_str:="";
            target_pos := CRobT(\Tool:=tVacuum1, \WObj:=wPalletIndex);
            pose_str := pose_str + NumToStr(target_pos.trans.x ,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.trans.y,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.trans.z,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q1,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q2,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q3,4) + ";";
            pose_str := pose_str + NumToStr(target_pos.rot.q4,4) + ";";
            pose_str := pose_str;
            Write file_vac_pallet, pose_str;
            
            pos_str:="";
            speed_str:="";
            torque_str:="";
            motor_torque_str:="";
            
            GetJointData \MechUnit:=ROB_L, 1 \Position:=position{1}\Speed:=speed{1}\Torque:=torques{1}\ExtTorque:=extTorques{1};
            GetJointData \MechUnit:=ROB_L, 2 \Position:=position{2}\Speed:=speed{2}\Torque:=torques{2}\ExtTorque:=extTorques{2};
            GetJointData \MechUnit:=ROB_L, 3 \Position:=position{3}\Speed:=speed{3}\Torque:=torques{3}\ExtTorque:=extTorques{3};
            GetJointData \MechUnit:=ROB_L, 4 \Position:=position{4}\Speed:=speed{4}\Torque:=torques{4}\ExtTorque:=extTorques{4};
            GetJointData \MechUnit:=ROB_L, 5 \Position:=position{5}\Speed:=speed{5}\Torque:=torques{5}\ExtTorque:=extTorques{5};
            GetJointData \MechUnit:=ROB_L, 6 \Position:=position{6}\Speed:=speed{6}\Torque:=torques{6}\ExtTorque:=extTorques{6};
            GetJointData \MechUnit:=ROB_L_7, 1 \Position:=position{7}\Speed:=speed{7}\Torque:=torques{7}\ExtTorque:=extTorques{7};
            
            motor_torque{1} := GetMotorTorque(\MecUnit:=ROB_L, 1);
            motor_torque{2} := GetMotorTorque(\MecUnit:=ROB_L, 2);
            motor_torque{3} := GetMotorTorque(\MecUnit:=ROB_L, 3);
            motor_torque{4} := GetMotorTorque(\MecUnit:=ROB_L, 4);
            motor_torque{5} := GetMotorTorque(\MecUnit:=ROB_L, 5);
            motor_torque{6} := GetMotorTorque(\MecUnit:=ROB_L, 6);
            motor_torque{7} := GetMotorTorque(\MecUnit:=ROB_L_7, 1);
            
            
            pos_str := pos_str + NumToStr(position{1},4) + ";";
            pos_str := pos_str + NumToStr(position{2},4) + ";";
            pos_str := pos_str + NumToStr(position{3},4) + ";";
            pos_str := pos_str + NumToStr(position{4},4) + ";";
            pos_str := pos_str + NumToStr(position{5},4) + ";";
            pos_str := pos_str + NumToStr(position{6},4) + ";";
            pos_str := pos_str + NumToStr(position{7},4) + ";";
            pos_str := pos_str;
            
            
            
            speed_str := speed_str + NumToStr(speed{1},4) + ";";
            speed_str := speed_str + NumToStr(speed{2},4) + ";";
            speed_str := speed_str + NumToStr(speed{3},4) + ";";
            speed_str := speed_str + NumToStr(speed{4},4) + ";";
            speed_str := speed_str + NumToStr(speed{5},4) + ";";
            speed_str := speed_str + NumToStr(speed{6},4) + ";";
            speed_str := speed_str + NumToStr(speed{7},4) + ";";
            speed_str := speed_str;

            
            
            torque_str := torque_str + NumToStr(torques{1},4) + ";";
            torque_str := torque_str + NumToStr(torques{2},4) + ";";
            torque_str := torque_str + NumToStr(torques{3},4) + ";";
            torque_str := torque_str + NumToStr(torques{4},4) + ";";
            torque_str := torque_str + NumToStr(torques{5},4) + ";";
            torque_str := torque_str + NumToStr(torques{6},4) + ";";
            torque_str := torque_str + NumToStr(torques{7},4) + ";";
            torque_str := torque_str;
            
            motor_torque_str := motor_torque_str + NumToStr(motor_torque{1},4) + ";";
            motor_torque_str := motor_torque_str + NumToStr(motor_torque{2},4) + ";";
            motor_torque_str := motor_torque_str + NumToStr(motor_torque{3},4) + ";";
            motor_torque_str := motor_torque_str + NumToStr(motor_torque{4},4) + ";";
            motor_torque_str := motor_torque_str + NumToStr(motor_torque{5},4) + ";";
            motor_torque_str := motor_torque_str + NumToStr(motor_torque{6},4) + ";";
            motor_torque_str := motor_torque_str + NumToStr(motor_torque{7},4) + ";";
            motor_torque_str := motor_torque_str;
    
            Write file1_l, pos_str;
            Write file2_l, speed_str;
            Write file3_l, torque_str;
            Write file4_l, motor_torque_str;
    
            
            counter := counter+1;
            IF counter = 6000 THEN
                exit_cycle := FALSE;
            ENDIF
            WaitTime LOGGER_PERIOD;
    	ENDWHILE
        Close file1_l;
        Close file2_l;
        Close file3_l;
        Close file4_l;
        Close file_pose;
        Close file_pose_pallet;
        Close file_vac_base;
        Close file_vac_pallet;
        ERROR (ERR_FILEOPEN)
        
    ENDPROC

ENDMODULE